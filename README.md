#RES Backend API

### Instructions for development

System setup:

```sh
$ sudo apt-get install python3.4
$ sudo apt-get install mongodb
$ sudo apt-get install python3-dev
$ sudo apt-get install virtualenv
```

The above should give all the dependencies for getting started.
Navigate to the directory where virtualenv will be created and run the following command

```sh
$ vritualenv -p <path_to_python3_bin> <v_env_name>
```
Now that we have the virtualenv set up with python 3, activate it and install requirements.

```sh
$ source <virtual_env_folder>/bin/activate
$ cd <git_repostitory>
$ pip install -r requirements.txt
```
With requirements install we can now start development.

First run the unit tests to see that everything is set up correctly.

```sh
$ ./runtests
```

To start the server use `./manage.py runserver`.
The default configuration used is *DevelopmentConfig*. This is overwritten by the above `FLASK_CONFIG` export. To revert to default, use

```sh
$ export FLASK_CONFIG=default
```

### Instructions for deployment

*Apache configuration required for redirecting API requests, see Apache documentation for details*

Set up the environment as described above.
For deployment we are going to be using a different application server.
To startup the application, instead of `./manage.py runserver' we will be using:

```sh
$ gunicorn manage:app
```

Gunicorn can take a number of configuration parameters, use `gunicorn -h` for details.
When deploying, make sure that the correct API configuration is used.

#### Reseting MongoDB

Once subjects are generated, they are stored in the database and only updated on the configured time intervals(see `config.py`).
**This means that the subjects returned, are not always output of current code.** If you need to force generation, drop the database.
To do that:

```sh
$ mongo
$ > show databases
$ > use <database_name>
$ > db.dropDatabase()
```

### Deploy with supervisor

In the scripts directory an example supervisor configuration is included.
On a Linux system, install supervisor using `apt-get install supervisor`.
Next modify the example config, and move it to `/etc/supervisor/conf.d/.`
Execute the following commands:

```sh
sudo supervisorctl reload
sudo supervisorctl update
sudo supervisorctl start res_backend
```
Supervisor will now start the service on system startup. http://supervisord.org/

#API Endpoints [GET]

By default the localhost address for the development server is:
`http://127.0.0.1:5000/api/v1.0/subject?uri=<dbpedia_uri>`

The currently deployed version on k-int is:

`http://resbe.k-int.com/subject?uri=<dbpedia_uri>`

The API also supports concept extraction from text, using the following URI:
`http://resbe.k-int.com/textMine?text=<text_content>`

#Resource



Current content types:

    purl_video = 'purl_moving_image'
    purl_image = 'purl_image'
    purl_desc = 'purl_description'
    purl_source = 'purl_source'
    foaf_page = 'foaf_page'
    purl_audio = 'purl_sound'


Example Subject (`http://resbe.k-int.com/subject?uri=http://dbpedia.org/resource/The_Beatles`):

```
#!PYTHON
{
   subject:"http://dbpedia.org/resource/The_Beatles",
   count:3,
   resources:[
      {... },
      {... },
      {
         contents:{
            purl_moving_image:[
               "http://devapi.bbcredux.com/2fd95e837ae746d4bc71a9af57db49ce/player"
            ],
            related:[
               {
                  contents:{
                     purl_moving_image:[
                        "http://devapi.bbcredux.com/3a66545c2dcb4d919732e67094aa7f27/player"
                     ],
                     purl_description:"A Hard Day's Night",
                     foaf_page:"http://www.bbc.co.uk/programmes/b0074q9m"
                  },
                  adapter:"RES",
                  title:"George Harrison"
               }
            ],
            purl_description:"George Harrison: Living in the...",
            foaf_page:"http://www.bbc.co.uk/programmes/b017lbr0"
         },
         adapter:"RES",
         title:"The Beatles"
      }
   ]
}
```

#Concepts
Example textMine output

```
#!PYTHON

{
  count: 2,
  concepts: [
    {
      name: "Treaty of Versailles",
      dbpedia: "http://dbpedia.org/resource/Treaty_of_Versailles"
    },
    {
      name: "World War I",
      dbpedia: "http://dbpedia.org/resource/World_War_I"
    },
  ]
}
```