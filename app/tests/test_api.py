import unittest
from res import create_app
from flask import current_app, url_for
import json
from manage import drop_db
from mongoengine import connect
from tests.client_setup import BaseClientTestCase
from unittest.mock import patch

class APITestCase(BaseClientTestCase):


    def test_lookup(self):
        response = self.client.get(url_for('api.lookup',uri="http://dbpedia.org/resource/Napoleon")    )
        response_values = json.loads(response.data.decode('utf-8'))
        self.assertTrue (len(response_values['resources']) == 31,"Instead found %d results" % len(response_values['resources'] ))

    def test_lookup_for_duplicates(self):

        response_one = self.client.get(url_for('api.lookup',uri="http://dbpedia.org/resource/Napoleon")    )
        response_two = self.client.get(url_for('api.lookup',uri="http://dbpedia.org/resource/Napoleon")    )
        response_values_one = json.loads(response_one.data.decode('utf-8'))
        response_values_two = json.loads(response_two.data.decode('utf-8'))
        self.assertTrue(response_values_two == response_values_one)

    @patch('res.api_1_0.api_endpoint.analyze')
    def test_concepts_extraction(self,mock_analyzer):
        mock_map = [x for x in range(0,12)]
        mock_analyzer.return_value = mock_map
        response = self.client.get(url_for('api.textMine',text=current_app.config.get('TEXT_ANALYZER_TEXT')))
        response_values = json.loads(response.data.decode('utf-8'))
        self.assertTrue(len(response_values['concepts']) == 12, "Instead got %d results "%len(response_values['concepts']) )
