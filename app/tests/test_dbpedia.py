import unittest
from res import create_app
from flask import current_app, url_for
import json
from res.domain import model
from manage import drop_db
from mongoengine import connect
from res.services.dbpedia_service import get_pref_label

class DBPediaTestCase(unittest.TestCase):

    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()
        self.setupDB()
        self.dbpedia_resource = current_app.config.get('DBPEDIA_RESOURCE')
        self.dbpedia_resource_label = current_app.config.get('DBPEDIA_RESOURCE_LABEL')

    def setupDB(self):
        drop_db()

    def tearDown(self):
        self.app_context.pop()

    def test_get_pref_label(self):
        self.assertTrue(get_pref_label(self.dbpedia_resource) == self.dbpedia_resource_label)


