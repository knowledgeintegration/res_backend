import unittest
import  unittest.mock
from unittest.mock import patch
from res import create_app
from flask import current_app, url_for
import json
from res.domain import model
from manage import drop_db
from mongoengine import connect
from res.services.programme_finder import lookup,get_programmes
from res.services import programme_finder
import datetime
from time import sleep
from testfixtures import LogCapture
import logging

class BaseClientTestCase(unittest.TestCase):


    def setUp(self):
        self.dbpedia_resource = current_app.config.get('DBPEDIA_RESOURCE')
        self.patcher_programmes = patch('res.services.programme_finder.get_programmes')
        self.mock_get_programmes = self.patcher_programmes.start()
        self.mock_get_programmes.return_value = self.create_mock_results()
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()
        self.setupDB()

    def setupDB(self):
        drop_db()

    def tearDown(self):
        self.patcher_programmes.stop()
        self.app_context.pop()


    def create_mock_results(self):
        purl_video = 'purl_moving_image'
        purl_image = 'purl_image'
        purl_desc = 'purl_description'
        purl_source = 'purl_source'
        foaf_page = 'foaf_page'

        resources = []
        for x in range(0,31):
            resource = dict.fromkeys(['title','source','snippet','link','image','adapter'],'')
            resource['title'] = "title %s" % x
            resource['adapter'] = "mocking API"
            resource['contents'] = {
                purl_video: x,
                purl_image:x,
                purl_desc:x,
                purl_source:x,
                foaf_page:x
            }
            resources.append(resource)
        return resources
