import unittest
from res import create_app
from flask import current_app
from res.services.res_crawler import find_all_subject_media, create_title

class ExternalAPITestCase(unittest.TestCase):

    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()
        self.dbpedia_resource = current_app.config.get('RES_DBPEDIA_RESOURCE')
        self.expected_res_link = current_app.config.get('EXPECTED_RES_RESULT')

    def tearDown(self):
        self.app_context.pop()

    def extract_values(self,resources):
        values = []
        for resource in resources:
            for k,v in resource['contents'].items():
                values.append(v[0])
        return values

    def test_create_title(self):
        title = create_title(self.dbpedia_resource)
        expected = 'The Taming of the Shrew'
        self.assertEqual(title, expected)

    def test_find_all_subject_media(self):
        """ RES Crawler test not very detailed but should be enough to know service works"""
        resources = find_all_subject_media(self.dbpedia_resource)

        vals = self.extract_values(resources)

        self.assertIn(self.expected_res_link, vals, "Expected RES link missing for %s" % self.dbpedia_resource)
        self.assertTrue("title" in resources[0],"Expected Key title missing from resource")
        self.assertTrue("contents" in resources[0],"Expected Key contents missing from resource")
        self.assertTrue(isinstance(resources[0]['contents'],dict),"Contents should be a map of media data")



