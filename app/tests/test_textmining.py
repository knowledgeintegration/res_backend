import unittest
from res import create_app
from flask import current_app, url_for
import json
from res.domain import model
from manage import drop_db
from res.services.text_mining_service import analyze

class TextMiningTestCase(unittest.TestCase):

    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()

    def tearDown(self):
        self.app_context.pop()

    def test_analyze(self):
        text = current_app.config.get('TEXT_ANALYZER_TEXT')
        concepts = analyze(text)
        self.assertTrue(len(concepts)==8)
        self.assertIn('http://dbpedia.org/resource/Napoleonic_Wars',(x['dbpedia'] for x in concepts))
        self.assertIn('French Revolution',(x['name'] for x in concepts))
