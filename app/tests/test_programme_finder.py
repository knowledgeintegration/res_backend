import unittest

import  unittest.mock
from unittest.mock import patch
import json
import datetime
import res
from time import sleep
from testfixtures import LogCapture
import logging
from tests.client_setup import BaseClientTestCase

class ProgrammeFinderTestCase(BaseClientTestCase):

    def test_lookup(self):
        lookup_result = res.services.programme_finder.lookup(self.dbpedia_resource)
        self.assertTrue(len(lookup_result.resources) == 31,"Instead found %d results" %len(lookup_result.resources))

    def test_get_programmes(self):
        result = res.services.programme_finder.get_programmes(None,self.dbpedia_resource)
        self.assertTrue(len(result) > 0 ,"Instead found %d results" %len(result))

    @patch('res.services.programme_finder._calc_update_interval')
    def test_refresh_interval(self, mock_function):
        dd = datetime.timedelta(milliseconds=10)
        earlier_date = datetime.datetime.now() - dd
        mock_function.return_value = earlier_date
        assert res.services.programme_finder._calc_update_interval() == earlier_date
        with LogCapture(level=logging.DEBUG) as l:
            res.services.programme_finder.lookup(self.dbpedia_resource)
            sleep(0.5)
            earlier_date = datetime.datetime.now() - dd
            mock_function.return_value = earlier_date
            res.services.programme_finder.lookup(self.dbpedia_resource)
        found_one = False
        found_two = False
        for record in l.records:
            if "Refreshing Subject: Subject(" in record.msg:
                if found_one:
                    found_two = True
                    break
                else:
                    found_one = True

        self.assertTrue(found_two,"Both lookups should have triggered a refresh")

    def test_refresh_interval_return(self):
        with LogCapture(level=logging.INFO) as l:
            res.services.programme_finder.lookup(self.dbpedia_resource)
            sleep(0.5)
            res.services.programme_finder.lookup(self.dbpedia_resource)

        first_refresh = False
        second_return = False

        for record in l.records:
            if "Refreshing Subject: Subject(" in record.msg:
                first_refresh = True
            if "not refreshing yet." in record.msg:
                second_return = True

        self.assertTrue(first_refresh and second_return ,"First lookup should generate, second return")




