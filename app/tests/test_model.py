import unittest
from res import create_app
from flask import current_app, url_for
import json
from res.domain.model import Resource,Subject
from manage import drop_db
from mongoengine import connect,NotUniqueError,MultipleObjectsReturned,DoesNotExist

class ModelTestCase(unittest.TestCase):

    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()
        self.setupDB()

    def setupDB(self):
        drop_db()

    def tearDown(self):
        self.app_context.pop()

    def test_createSubject(self):
        subj = Subject(uri="hello").save()
        self.assertTrue(subj.uri == "hello")


    def test_subject_uri_unique(self):
        subj = Subject(uri="hello").save()
        with self.assertRaises(NotUniqueError):
            subj_2 = Subject(uri="hello").save()


    def test_find_unique(self):
        subj = Subject(uri="hello").save()
        found = Subject.objects.get(uri="hello")
        self.assertTrue(subj == found)

    # def test_resource_update_or_create(self):
    #     Resource.objects(uri='hello').update_one(set__title='black',upsert=True)
    #     self.assertTrue( Resource.objects.get(uri='hello').title == 'black')

    def test_subject_create_new(self):
        subj = Subject.objects(uri="hello").modify(set__uri="hello",upsert=True)
        self.assertTrue(subj == None)

    def test_lookup_uri_not_exist(self):
        find_result = Subject.objects(uri='hello')
        self.assertTrue(len(find_result) == 0)

