import os


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'hard to guess string'
    SSL_DISABLE = True
    DBPEDIA_URI = "http://dbpedia.org/sparql/"
    RES_URI = "http://acropolis.org.uk"
    GOOGLE_API_URI = "https://www.googleapis.com/customsearch/v1"
    CG_API_URI = "http://www.culturegrid.org.uk/discover/xsl/default"
    TEXT_ANALYZER = "http://access.alchemyapi.com/calls/text/TextGetRankedConcepts"
    # Calculated in days
    RESOURCE_UPDATE_INTERVAL = 5

    @staticmethod
    def init_app(app):
        pass


class StagingConfig(Config):
    DEBUG = False
    MONGO_DB_NAME = "res_stage_database"

    @classmethod
    def init_app(cls, app):
        import logging
        from logging.handlers import RotatingFileHandler
        rotating_handler = RotatingFileHandler(filename='gunicorn.out', maxBytes=10000000, backupCount=5)
        formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
        rotating_handler.setFormatter(formatter)
        app.logger.addHandler(rotating_handler)
        app.logger.setLevel(logging.INFO)
        app.logger.info("Using StagingConfig")


class TestingConfig(Config):
    TESTING = True
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    MONGO_DB_NAME = "res_test_database"
    DBPEDIA_RESOURCE = "http://dbpedia.org/resource/The_Beatles"
    DBPEDIA_RESOURCE_LABEL = "The Beatles"
    RES_DBPEDIA_RESOURCE = "http://dbpedia.org/resource/The_Taming_of_the_Shrew"
    # Following is a link that we expect to be returned for The_Beatles from res.
    EXPECTED_RES_RESULT = "http://shakespeare.acropolis.org.uk/programmes/LMAC282R/player"
    TEXT_ANALYZER_TEXT = """ Napoléon Bonaparte (/nəˈpoʊliən, -ˈpoʊljən/;[2] French: [napɔleɔ̃ bɔnapaʁt],
        born Napoleone di Buonaparte; 15 August 1769 – 5 May 1821) was a French military and political leader
who rose to prominence during the French Revolution and its associated wars. As Napoleon I, he was Emperor of the
French from 1804 until 1814, and again in 1815. Napoleon dominated European affairs for nearly two decades
while leading France against a series of coalitions in the Revolutionary Wars and the Napoleonic Wars.
He won the large majority of his 60 major battles and seized control of most of continental Europe before
his ultimate defeat in 1815. One of the greatest commanders in history, his campaigns are studied at military
schools worldwide and he remains one of the most celebrated and controversial political figures in Western history.[3][4]
 In civil affairs, Napoleon implemented several liberal reforms across Europe, including the abolition of feudalism,
 the establishment of legal equality and religious toleration, and the legalization of divorce. His lasting legal
 achievement, the Napoleonic Code, has been adopted by dozens of nations around the world. """

    @classmethod
    def init_app(cls, app):
        print("Using TestingConfig")


class DevelopmentConfig(Config):
    DEBUG = True
    MONGO_DB_NAME = "res_dev_database"
    RESOURCE_UPDATE_INTERVAL = 0

    @classmethod
    def init_app(cls, app):
        import logging
        from logging import StreamHandler
        stream_handler = StreamHandler()
        formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
        stream_handler.setFormatter(formatter)
        app.logger.addHandler(stream_handler)
        app.logger.setLevel(logging.DEBUG)
        app.logger.debug("Using DevelopmentConfig")


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'staging': StagingConfig,
    'default': DevelopmentConfig
}
