#!/usr/bin/env python
from res import create_app
from flask.ext.script import Manager
from flask import current_app
from mongoengine import connect
import os

if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]


app = create_app(os.getenv('FLASK_CONFIG') or 'default')
manager = Manager(app)


@manager.command
def test(coverage=False, testcase=None):
    """Run the unit tests."""
    if coverage and not os.environ.get('FLASK_COVERAGE'):
        import sys
        os.environ['FLASK_COVERAGE'] = '1'
        os.execvp(sys.executable, [sys.executable] + sys.argv)
    import unittest
    suite = None
    if testcase:
        import importlib
        test_module = importlib.import_module("tests.%s" % testcase)
        suite = unittest.TestLoader().loadTestsFromModule(test_module)
    else:
        suite = unittest.TestLoader().discover('tests')

    unittest.TextTestRunner(verbosity=2).run(suite)


@manager.command
def drop_db():
    """Drop the database provided in the config"""
    db_name = current_app.config.get('MONGO_DB_NAME')
    db = connect(db_name)
    db.drop_database(db_name)


if __name__ == '__main__':
    manager.run()
