from flask import make_response, request, current_app
from bson.json_util import dumps
from . import api
from res.services.programme_finder import lookup as programme_lookup
from res.services.text_mining_service import analyze


@api.route('/subject', methods=['GET'])
def lookup():
    """ Given a uri, return some data"""
    uri = request.args.get('uri')
    if(uri and len(uri) > 0):
        current_app.logger.info("Received URI %s" % uri)
        subject = programme_lookup(uri)

        # Models are handling the output format
        json_out = subject.to_map()

        return make_response(dumps(json_out), 200, {"Content-type": "application/json"})


@api.route('/textMine', methods=['GET'])
def textMine():
    text = request.args.get('text')
    concepts = analyze(text)
    result_json = {'concepts': concepts}
    result_json['count'] = len(concepts)
    return make_response(dumps(result_json), 200, {"Content-type": "application/json"})
