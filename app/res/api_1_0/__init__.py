from flask import Blueprint

api = Blueprint('api', __name__)

from . import api_endpoint  # noqa: E402,F401
