from res.domain.model import Subject, Resource
from flask import current_app
import datetime
from res.services import res_crawler as crawler


def lookup(uri):

    update_interval = _calc_update_interval()
    # when creating new subject, set the date past interval to force update
    subject = (Subject.objects(uri=uri)
            .modify(new=True, set_on_insert__date_modified=update_interval, set__uri=uri, upsert=True))

    if subject.date_modified <= update_interval:
        current_app.logger.info("Refreshing Subject: %s " % subject)
        programmes = get_programmes(None, uri)
        subject.update(unset__resources=None)
        for p in programmes:
            resource = Resource(title=p['title'], adapter=p['adapter'], contents=p['contents'])
            subject.update(add_to_set__resources=resource)

        subject.update(set__date_modified=datetime.datetime.now())
        subject.reload()
    else:
        current_app.logger.info("Subject %s updated on %s, not refreshing yet." % (uri, subject.date_modified))

    return subject


def _calc_update_interval():
    today = datetime.datetime.now()
    # The days we want to subtract for today.
    update_interval = current_app.config.get("RESOURCE_UPDATE_INTERVAL")
    dd = datetime.timedelta(days=update_interval)
    # Find which date it was x days ago
    earlier_date = today - dd
    return earlier_date


def get_programmes(pref_label, uri, count=10):
    collective = []

    res_crawler = crawler.find_all_subject_media(uri)

    collective.extend(res_crawler)

    current_app.logger.info("Merge %d elements from RES Crawker" % len(res_crawler))
    # current_app.logger.debug(str(res_crawler))

    return collective
