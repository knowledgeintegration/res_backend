from flask import current_app
import requests


def analyze(text):
    current_app.logger.info("Analyze text %s" % text)
    api_uri = current_app.config.get('TEXT_ANALYZER')
    req_params = {
        'text': text,
        'apikey': 'd701b5db3383a93e38069cb4472c5c1fca2214b5',
        'outputMode': 'json'
    }
    r = requests.get(api_uri, params=req_params)

    responce_json = r.json()
    concepts = []

    for concept in responce_json['concepts']:
        if concept.get('dbpedia', None) is not None:
            concept_map = {}
            concept_map['dbpedia'] = concept.get('dbpedia', '')
            concept_map['name'] = concept.get('text', '')
            concepts.append(concept_map)

    return concepts
