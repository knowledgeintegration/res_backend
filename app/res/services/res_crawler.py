from rdflib import URIRef, Graph
from rdflib.namespace import RDF, RDFS, FOAF, OWL
from rdflib.plugins.sparql import prepareQuery
from flask import current_app
from urllib.error import HTTPError

purl_episode = URIRef("http://purl.org/ontology/po/Episode")
purl_work = URIRef("http://purl.org/vocab/frbr/core#Work")
purl_audio = URIRef("http://purl.org/ontology/mo/Sound")


def create_queries():
    """ Called on app startup to generate the static queries"""
    # Create the queries here
    # Have the media optional, but at least one must be present
    media_link_query = """
           SELECT DISTINCT ?label ?page (group_concat(distinct ?topic;separator=";") as ?topics) ?link ?link_two
           WHERE {
              ?subj rdf:type ?episode .
              ?subj rdf:type ?work .
              OPTIONAL { ?subj rdfs:label ?label . }
              OPTIONAL { ?subj foaf:topic ?topic . }
              OPTIONAL { ?subj <http://search.yahoo.com/mrss/player> ?link . }
              OPTIONAL { ?subj <http://search.yahoo.com/mrss/content> ?link_two . }
              OPTIONAL { ?subj foaf:page ?page}
           }"""

    media_link_q = prepareQuery(media_link_query, initNs={"rdf": RDF, "rdfs": RDFS, "foaf": FOAF})

    # We assume that these two values mark every creative work we are interested in
    creative_work_query = """
           SELECT DISTINCT ?subj
           WHERE {
              ?subj rdf:type ?episode .
              ?subj rdf:type ?work .
           }"""
    creative_work_q = prepareQuery(creative_work_query, initNs={"rdf": RDF})

    return creative_work_q, media_link_q


def find_creative_work_links(episode_match, resources, title, primaryTopic=None):
    """ This is inside the creative work graph, we just need to grap the content and desc"""
    g_media = Graph().parse(episode_match)

    video_link = g_media.query(media_link_q, initBindings={
        "subj": episode_match,
        "episode": purl_episode,
        "work": purl_work})

    visited_topics = [primaryTopic] if primaryTopic else []
    for row in video_link:
        # No media no reason to have resource
        if row[0] is None and row[2] is None:
            continue
        related_resources = []
        # Check for primary or we go in recursion
        if primaryTopic:
            related_topics = row[2].split(';')
            for related_topic in related_topics:
                if related_topic not in visited_topics:
                    # Mark topic as viewed and lookup for media
                    visited_topics.append(related_topic)
                    find_subject_works(related_topic, None, related_resources)

        # if its sound change media type
        media_type = "purl_moving_image"
        if (episode_match, RDF.type, purl_audio) in g_media:
            media_type = "purl_sound"
        # We got all the required data so generate the resource
        new_resource = generate_resource(title, related_resources, resources, row, media_type)
        # After the duplicate checks, we may get a null back so check it
        if new_resource:
            resources.append(new_resource)


def generate_resource(title, related_resources, resources, row, media_type):
    """ Generate a new resource, and make sure we do not create duplicates """
    # Put all matched media links in a list
    media_links = [row[i].toPython() for i in (3, len(row) - 1) if row[i] is not None]

    # If any links already in the resources, remove them
    # This is effective for avoiding duplicates in related resources
    check_for_duplicates(resources, media_links, media_type)

    if media_links:
        resource = dict.fromkeys(['title', 'adapter', 'contents'], '')
        resource['adapter'] = "RES"
        if title is not None:
            resource['title'] = title
        resource['contents'] = {}
        # Label is the first item we select
        if row[0] is not None:
            resource['contents']['purl_description'] = row[0].toPython()
        # Page is after label
        if row[1] is not None:
            resource['contents']['foaf_page'] = row[1].toPython()
        # Then we got any media links
        resource['contents'][media_type] = media_links

        if not len(related_resources) == 0:
            # This is to make sure related resources are not pointing to the same resource as primary
            check_for_duplicates(related_resources, media_links, media_type, True)
            if not len(related_resources) == 0:
                resource['contents']['related'] = related_resources

        return resource

    return None


def check_for_duplicates(resources, media_links, media_type, primary_resource=False):
    """ Remove any duplicate links/resources. Used in the scope of a single resource and its related resources"""
    for existing_resource in resources:
        if media_type in existing_resource['contents']:
            for existing_media in existing_resource['contents'][media_type]:
                if existing_media in media_links:
                    if primary_resource:
                        # If we work with primary, we want to remove the related resource links
                        existing_resource['contents'][media_type].remove(existing_media)
                        if len(existing_resource['contents'][media_type]) == 0:
                            # If all links removed remove the resource
                            resources.remove(existing_resource)
                    else:
                        # A related resource already points to this media, so remove the link
                        media_links.remove(existing_media)


def find_subject_works(subject_uri, dbpedia_uri, resources, related=False):
    """ Find all creative works, and individually enter and pull out resources from them """
    g_subject = None
    try:
        g_subject = Graph().parse(subject_uri)
    except (HTTPError, AttributeError):
        # HTTP error when wrong url, AttributeError when wrong url but we get xml back
        current_app.logger.exception("No RES results for %s" % subject_uri)
        return None

    primaryTopic = None
    if related:
        for topic in g_subject.subjects(FOAF.primaryTopic, None):
            # We need to know primary topic, for when we search related topics
            if current_app.config.get("RES_URI") in topic.toPython():
                primaryTopic = topic.toPython() + '#id'

    if dbpedia_uri is None:
        # If no dbpedia link just try preferredLabel, its hit/miss
        pref_label = g_subject.preferredLabel(URIRef(subject_uri), "en")
        title = pref_label[0][1] if pref_label else None
    else:
        # Find the Title to use based on the dbpedia label
        dbpedia_uri_ref = URIRef(dbpedia_uri)
        graph_subject = g_subject.value(None, OWL.sameAs, dbpedia_uri_ref)
        title = extract_title(g_subject, graph_subject, dbpedia_uri)

    creative_work_results = g_subject.query(creative_work_q,
        initBindings={'episode': purl_episode, 'work': purl_work})
    for row in creative_work_results:
        if(row[0].endswith('#id')):
            episode_match = row[0]
            find_creative_work_links(episode_match, resources, title, primaryTopic)


def find_all_subject_media(dbpedia_uri):
    """ Main entry point for the crawler. Using a dbpedia uri find all available media and return them"""
    resources = []

    # This is the link generated from the incoming request
    res_base_uri = current_app.config.get('RES_URI')
    lookup_uri = res_base_uri + "/?uri=" + dbpedia_uri
    current_app.logger.debug("Lookup graph for: %s " % lookup_uri)

    find_subject_works(lookup_uri, dbpedia_uri, resources, True)

    return resources


def create_title(dbpedia_uri):
    """ Create a resource title from the dbpedia link if we cant find a label """
    idx = dbpedia_uri.index('/resource/') + len('/resource/')
    title = dbpedia_uri[idx:]
    # If we come across any more chars that need replacing add them here
    title = title.replace('_', ' ')

    return title


def extract_title(g_subject, graph_subject, dbpedia_uri):
    """ Need some processing to get our resource title """

    try:
        # This returns generator, but there should only be one value
        title = next(g_subject.objects(graph_subject, RDFS.label), dbpedia_uri)
    except:
        # Dont think this can ever happen
        title = None
        current_app.logger.exception("Unexpected exception for resource: %s" % dbpedia_uri)

    title = create_title(dbpedia_uri) if title is None else title

    return title.replace('\n', ' ').replace('\r', '')


# Compile SPARQL queries once on server startup
(creative_work_q, media_link_q) = create_queries()
