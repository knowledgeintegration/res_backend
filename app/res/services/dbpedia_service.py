import requests
from flask import current_app


def get_pref_label(uri):
    prefLabel = None

    dbpedia_uri = current_app.config.get('DBPEDIA_URI')
    req_params = {
        'query': "select ?o where { <%s> <http://www.w3.org/2000/01/rdf-schema#label> ?o }" % uri,
        'format': 'application/sparql-results+json',
        'timeout': 0
    }

    r = requests.get(dbpedia_uri, params=req_params)

    responce_json = r.json()
    for value in responce_json['results']['bindings']:
        if value['o']['xml:lang'] == 'en':
            prefLabel = value['o']['value']
            break
    return prefLabel
