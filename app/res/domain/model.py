import mongoengine
import datetime


class Resource(mongoengine.EmbeddedDocument):
    title = mongoengine.StringField()
    adapter = mongoengine.StringField()
    contents = mongoengine.DictField(required=True)

    def __str__(self):
        return 'Resource(title=%s,adapter=%s,contents=%s)' % (self.title, self.adapter, str(self.contents))

    def to_map(self):
        json_out = {
            'title': self.title,
            'adapter': self.adapter,
            'contents': self.contents
        }
        return json_out


class Subject(mongoengine.Document):
    uri = mongoengine.StringField(required=True, unique=True)
    resources = mongoengine.EmbeddedDocumentListField('Resource')
    date_modified = mongoengine.DateTimeField(default=datetime.datetime.now)

    meta = {
        'indexes': [
            {'fields': ['uri'], 'unique': True},
        ],
    }

    def __str__(self):
        return 'Subject(uri=%s, resources=List(%d),date_modified=%s)' % (self.uri, len(self.resources), self.date_modified)

    def to_map(self):
        json_out = {
            'subject': self.uri,
            'resources': [],
            'count': len(self.resources)
        }

        for resource in self.resources:
            json_out['resources'].append(resource.to_map())

        return json_out
