#!/bin/bash
cd ..
source res_env/bin/activate
cd app
export FLASK_CONFIG=staging
exec gunicorn -b '192.168.1.10:8000' -p 'gunicorn.pid' manage:app
